import 'package:flutter/material.dart';
import '../../../../helper/Colors.dart';
import '../../../../helper/fontStyle.dart';
import '../widgets/tab_bar_page.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key,}) : super(key: key);


  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {


  int _currentPage = 0;
  final _pageController = PageController();
  final List<MyTabItem> items = [
    MyTabItem('Transactions', Icons.mobile_screen_share),
    MyTabItem('Categories', Icons.business),
    MyTabItem('Payments', Icons.payment),
    MyTabItem('Piggy Bank', Icons.monetization_on_outlined),
    MyTabItem('More', Icons.more_horiz),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: PageView(
        controller: _pageController,
        children: [
          TabBarPage(),
          Container(color: Colors.red),
          Container(color: Colors.greenAccent.shade700),
          Container(color: Colors.orange),
          Container(color: Colors.blue),

        ],

        onPageChanged: (index) {
          setState(() => _currentPage = index);
        },
      ),
        bottomNavigationBar: bottomBar(context),
    );
  }

  List<BottomNavigationBarItem> getBottomTabs( List<MyTabItem> tabs) {
    return tabs
        .map(
          (item) =>
          BottomNavigationBarItem(
            icon: Icon(item.icon),
            label: item.title,
          ),
    )
        .toList();
  }

  //METHOD TO SET STATE
  void _onItemTapped(int index) {
    _pageController.jumpToPage(index);
    setState(() => _currentPage = index);
  }

 Widget bottomBar(BuildContext context) {
    return BottomNavigationBar(iconSize: 32,
       backgroundColor: Colors.grey.shade300,
        type: BottomNavigationBarType.fixed,
        items: getBottomTabs(items),
        selectedLabelStyle:  boldStyle(fontSize: 13.6,color:grayBoldColor),

        unselectedLabelStyle:boldStyle(fontSize: 13.6,color:grayLightColor) ,
        selectedItemColor: blackColor,
        unselectedItemColor:blackColor.withOpacity(0.7) ,

        //THIS METHOD NEEDS TO BE CALLED TO CHANGE THE STATE
        onTap: _onItemTapped,

        currentIndex: _currentPage);
 }
}
class MyTabItem {

  String? title;
  IconData? icon;
  MyTabItem(this.title, this.icon);

}
