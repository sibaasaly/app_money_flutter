
import 'package:app_money/helper/Colors.dart';
import 'package:app_money/helper/constant.dart';
import 'package:flutter/material.dart';

import '../../../../helper/fontStyle.dart';
import 'first_page_ms.dart';

class PageItem extends StatefulWidget {
  final int index;
  const PageItem(this.index, {Key? key}) : super(key: key);

  @override
  _PageItemState createState() => _PageItemState();
}

class _PageItemState extends State<PageItem>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      // alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [ Colors.grey.shade100, Colors.grey.shade300],
            begin: Alignment.centerRight,
            end: Alignment.bottomRight,
            stops: const [0.0, 0.8],
            tileMode: TileMode.clamp,
          ),
        ),
      child:Padding(
        padding:  EdgeInsets.only(left: Constant.valuePaddingEdges+1,
        right: Constant.valuePaddingEdges+1),
        child: containPage(widget.index),
      ),
    );
  }

  @override
  // bool get wantKeepAlive => false;
  bool get wantKeepAlive => true;

   Widget containPage(int index) {
    if(index!=0) {
      return    Center(
        child: Text('*___* ${widget.index}',style: boldStyle(
    color: blackColor,
    fontSize: 16)),
      );
    } else {
    return const FirstPageMS();
    }
  }
}