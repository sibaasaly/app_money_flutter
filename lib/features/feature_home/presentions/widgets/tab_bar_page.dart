import 'package:app_money/features/feature_home/presentions/widgets/page_item.dart';
import 'package:app_money/helper/fontStyle.dart';
import 'package:app_money/helper/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_tab_bar/library.dart';

import '../../../../helper/Colors.dart';

class TabBarPage extends StatefulWidget {
  const TabBarPage({Key? key}) : super(key: key);

  @override
  _TabBarPageState createState() => _TabBarPageState();
}

class _TabBarPageState extends State<TabBarPage> {
  final int pageCount = 5;
  List<String> items = ["MS", "Accounts", "Budget", "Analytics", "Finance", ];
  late final PageController _controller = PageController(initialPage: 0);
  final CustomTabBarController _tabBarController = CustomTabBarController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Column(

        children: [
          SizedBox(height: MediaQuery.of(context).size.height*0.05),
          Padding(
            padding:  EdgeInsets.all(Constant.valuePaddingEdges),
            child: CustomTabBar(
              tabBarController: _tabBarController,
              height: 60,
              itemCount: pageCount,
              builder: getTabBarChild,

              indicator: RoundIndicator(
                color: orangeLightColor,
                top: 2.0,
                bottom: 2.0,
                left: 2.0,
                right: 2.0,
                radius: BorderRadius.circular(95),
              ),
              pageController: _controller,
            ),
          ),
          Expanded(
              child: PageView.builder(
                  controller: _controller,
                  itemCount: pageCount,
                  itemBuilder: (context, index) {
                    return Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: grayLightColor.withOpacity(0.3),
                              width: 1,
                            )),
                        child: PageItem(index));
                  })),
        ],
      ),
    );
  }

  Widget getTabBarChild(BuildContext context, int index) {

    return TabBarItem(
        transform: ColorsTransform(
            highlightColor: Colors.white,
            normalColor: Colors.black,
            builder: (context, color) {
              return Container(
                  padding: const EdgeInsets.fromLTRB(10, 2, 10, 2),
                  alignment: Alignment.center,

                  constraints: const BoxConstraints(minWidth: 60),
                  child:Center(
                    child: Text("${items[index]}",
                      style: boldStyle(fontSize: 15.6,color:blackColor),
                    ),
                  )


              );
            }),
        index: index);
  }
}