

import 'package:app_money/helper/Colors.dart';
import 'package:app_money/helper/fontStyle.dart';
import 'package:flutter/material.dart';

class FirstPageMS extends StatefulWidget {
  const FirstPageMS({Key? key}) : super(key: key);


  @override
  State<FirstPageMS> createState() => _FirstPageMSState();
}

class _FirstPageMSState extends State<FirstPageMS> {


 final List data = [
   {'date': '15.02.2021',
     'price': '-\$24.00',
     'details':[
       {'title':'Starbucks','date_detail':'12:46,Feb 15,2021','price_detail':'-\$5.49','phone':'**** **** **** **34',
       'image':'https://images.unsplash.com/photo-1547721064-da6cfb341d50'},
       {'title':'Asda','date_detail':'09:15,Feb 15,2021','price_detail':'-\$18.51','phone':'**** **** **** **68',
         'image':'https://images.unsplash.com/photo-1547721064-da6cfb341d50?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80' },
     ]
   },
   {'date': '14.02.2021',
     'price': '-\$47.33',
     'details':[
       {'title':'Sold GBP to Bitcoin','date_detail':'18:11,Feb 14,2021','price_detail':'-\$149.99', 'phone':'**** **** **** **68',
         'image':'https://picsum.photos/250?image=9'},
       {'title':'Netflix','date_detail':'16:59,Feb 14,2021','price_detail':'-\$9.99','phone':'**** **** **** **34',
         'image':'https://picsum.photos/250?image=9'},
       {'title':'Bank transfer','date_detail':'12:01,Feb 14,2021','price_detail':'-\$120.00','phone':'**** **** **** **58',
         'image':'https://images.pexels.com/photos/213780/pexels-photo-213780.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'},
       {'title':'Zara','date_detail':'09:35,Feb 14,2021','price_detail':'-\$250.00','phone':'**** **** **** **59',
         'image':'https://images.unsplash.com/photo-1547721064-da6cfb341d50'},
     ]
   },
   {'date': '13.02.2021',
     'price': '-\$47.33',
     'details':[
       {'title':'Prime video','date_detail':'01:46,Feb 13,2021','price_detail':'-\$75.49','phone':'**** **** **** **04',
         'image':'https://cdn.pixabay.com/photo/2013/05/11/08/28/sunset-110305_1280.jpg'},
       {'title':'Amazon','date_detail':'07:26,Feb 13,2021','price_detail':'-\$54.49','phone':'**** **** **** **32',
         'image':'https://images.pexels.com/photos/213780/pexels-photo-213780.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'},
     ]
   },
 ];
 List _foundUsers = [];

    bool visible=false;


 @override
 initState() {
   // at the beginning, all users are shown
   _foundUsers = data;
   super.initState();
 }
  @override
  Widget build(BuildContext context) {
    return Container(
     child: Flex(
       direction: Axis.vertical,
          children: [
           searchTransaction(context),
           groupedListItemsDate(context)
          ],
     ),
    );
  }




 Widget searchTransaction(BuildContext context) {
    return   Flex(
      direction: Axis.vertical,
      children: [
        Flex(
          direction: Axis.horizontal,
          mainAxisAlignment:MainAxisAlignment.spaceBetween ,
          children: [
            Flex(
              direction: Axis.horizontal,
              children: [
                Text("Transactions",style: boldStyle(color: blackColor,fontSize: 14.6),),
                IconButton(onPressed: (){
                  setState(() {
                    visible = !visible;
                    _foundUsers = data;
                  });
                }, icon: Icon(Icons.search,color: grayLightColor,))
              ],
            ),
            Flex(
              direction: Axis.horizontal,
              children: [
                Text("Cashflow this week: ",style: boldStyle(color: blackColor,fontSize: 14.6),),
                Text("Good",style: boldStyle(color: greenColor,fontSize: 14.6),),
                Text("-\$250",style: boldStyle(color: blackColor,fontSize: 14.6),),
              ],
            ),
          ],
        ),
        Visibility(
          visible:visible,

          child:  TextField(
           onChanged: (value) => _runFilter(value),
            decoration: InputDecoration(
              labelStyle: normalStyle(color: grayBoldColor,fontSize: 17),
                labelText: 'Search',
              enabledBorder:  const UnderlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.black
                ),
              ),
              focusedBorder: const UnderlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.black
                ),
              ),
                ),
          ),
        ),

        const SizedBox(height: 20,),
      ],
    );
  }

 void _runFilter(String enteredKeyword) {
   debugPrint("enteredKeyword $enteredKeyword");
   List results = [];
   if (enteredKeyword.isEmpty) {
     // if the search field is empty or only contains white-space, we'll display all users
     results = data;
     debugPrint("enteredKeyword.isEmpty result $results");

   } else {
     results = data
         .where((data) =>
         data["date"].contains(enteredKeyword))
         .toList();
     debugPrint("enteredKeyword.isNotEmpty result $results");

     // we use the toLowerCase() method to make it case-insensitive
   }
   // debugPrint("");

   // Refresh the UI
   setState(() {
     _foundUsers = results;

     debugPrint("_foundUsers $_foundUsers");
   });
 }

  Widget  groupedListItemsDate(BuildContext context) {
    return Flexible( fit: FlexFit.tight,
      child: _foundUsers.isNotEmpty
          ? ListView.builder(
        shrinkWrap: true,
        itemCount: _foundUsers.length,
        itemBuilder: (context, index) {
          return Flex(
            direction: Axis.vertical,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment:CrossAxisAlignment.start ,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 5),
              Flex(
                direction: Axis.horizontal,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
               // crossAxisAlignment:CrossAxisAlignment.start ,
                children: [
                Text(_foundUsers[index]['date'],style: boldStyle(fontSize: 15.0,color: grayBoldColor),),
                Text(_foundUsers[index]['price'],style: boldStyle(fontSize: 15.0,color: grayBoldColor),),
              ],),

              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: _foundUsers[index]['details'].length,
                itemBuilder: (context, ind){
                  return Card(
                    elevation: 0.0,
                      shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                  side: BorderSide(width: 1, color: blackColor)),
                    color: Colors.transparent,

                    margin:
                    const EdgeInsets.symmetric(horizontal: 0, vertical: 6.0),
                    child: DecoratedBox (
                      decoration: BoxDecoration(
                        borderRadius:BorderRadius.all(Radius.circular(60)) ,
                        gradient: LinearGradient(
                          colors: [ Colors.grey.shade100, Colors.grey.shade300],
                          begin: Alignment.centerRight,
                          end: Alignment.bottomRight,
                          stops: [0.0, 0.8],
                          tileMode: TileMode.clamp,
                        ),
                      ),
                      child: ListTile(
                        minLeadingWidth: 0,

                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: -6),
                        leading: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundImage: NetworkImage('${_foundUsers[index]['details'][ind]['image']}'),
                              backgroundColor: Colors.grey.shade500,
                              radius: 24,
                            )
                          ],
                        ),
                        title: Flex(
                          direction: Axis.horizontal,
                          mainAxisAlignment:MainAxisAlignment.spaceBetween ,
                          children: [
                            Text(_foundUsers[index]['details'][ind]['title'],
                                style: boldStyle(color: blackColor,fontSize: 16.5)),
                        Text(_foundUsers[index]['details'][ind]['price_detail'],
                                  style: boldStyle(fontSize: 15.0,color: redColor)),
                          ],
                        ),
                        subtitle: Flex(
                          direction: Axis.horizontal,
                          mainAxisAlignment:MainAxisAlignment.spaceBetween ,
                          children: [
                            Text(_foundUsers[index]['details'][ind]['date_detail'],
                            style: boldStyle(fontSize: 11.0,color: grayBoldColor)),

                        Text(_foundUsers[index]['details'][ind]['phone'],
                                   style: boldStyle(fontSize: 11.0,color: blackColor))
                          ],
                        ),

                      ),
                    ),
                  );
                })
            ],
          );
        },
      ):
      Padding(
        padding: const EdgeInsets.all(15.0),
        child:  Text(
          'No results found *___*',
          style: boldStyle(fontSize: 17,color: grayBoldColor),
        ),
      ),
    );
  }

}


