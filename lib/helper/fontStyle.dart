import 'package:flutter/cupertino.dart';

TextStyle normalStyle({double? fontSize, Color? color}){
  return TextStyle(fontSize: fontSize,color: color,fontWeight: FontWeight.normal,height: 1.2);
}

TextStyle boldStyle({double? fontSize, Color? color}){
  return TextStyle(fontSize: fontSize,color: color,fontWeight: FontWeight.bold,height:1.2);
}