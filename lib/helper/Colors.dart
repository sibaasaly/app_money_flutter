import 'package:flutter/material.dart';


Color orangeLightColor=Colors.amberAccent;

Color colorShadow=Colors.grey.shade300;

Color blackColor=Colors.black;

Color grayBoldColor=Colors.grey.shade600;

Color grayLightColor=Colors.grey.shade500;

Color redColor=Colors.red;

Color greenColor=Colors.green.shade600;

Color whiteColor=Colors.white;

